import React, { Component } from "react";
import "./App.css";
import NavBar from "./Components/NavBarComponent";
import HotelListPage from "./Pages/HotelListPage";
import { Provider } from "react-redux";
import store from "./store";

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <div>
          <NavBar />
          <HotelListPage />
        </div>
      </Provider>
    );
  }
}

export default App;
