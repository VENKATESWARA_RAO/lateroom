import React, { Component } from "react";

class NavBar extends Component {
  render() {
    return (
      <div>
        <nav className="navbar navbar-inverse navbar-fixed-top">
          <div className="container-fluid">
            <div className="navbar-header">
              <a className="navbar-brand">
                LateRoom
              </a>
            </div>           
          </div>
        </nav>
      </div>
    );
  }
}

export default NavBar;
