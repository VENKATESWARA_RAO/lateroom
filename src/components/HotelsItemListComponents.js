import React, { Component } from "react";
import PropTypes from "prop-types";

class HotelListItemComponents extends Component {
  static propTypes = {
    HotelList: PropTypes.array
  };
  render() {
    let keyValue = 1;
    const HotelListItems = this.props.HotelList.map(HotelList => {
      if (HotelList) {
        const constHotelListFacilities = HotelList.facilities.map(
          facilities => {
            return <span  key={keyValue++}> {facilities}, </span>;
          }
        );
        return (
          <tr key={keyValue++}>
            <td>{HotelList.name}</td>
            <td>{HotelList.starRating}</td>
            <td>{constHotelListFacilities}</td>
          </tr>
        );
      }
    });
    return <tbody>{HotelListItems}</tbody>;
  }
}

export default HotelListItemComponents;
