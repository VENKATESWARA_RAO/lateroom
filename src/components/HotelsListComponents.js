import React, { Component } from "react";
import HotelListItemComponents from "./HotelsItemListComponents";
import PropTypes from 'prop-types'

class HotelListComponents extends Component {

  static propTypes = {    
    HotelList: PropTypes.array    
  }

  render() {  
    return ( 
          <table className="table">
            <thead>
              <tr>
                <th onClick={this.props.onSort.bind(this, 'Name')}>Name</th>
                <th onClick={this.props.onSort.bind(this, 'StarRating')}>StarRating</th>
                <th onClick={this.props.onSort.bind(this, 'Facilities')}>Facilities</th>
              </tr>
            </thead>     
            <HotelListItemComponents HotelList ={this.props.HotelList} />      
          </table>
          
    );
  }
}

export default HotelListComponents;
