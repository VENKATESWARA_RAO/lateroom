import React, { Component } from "react";
import HotelListComponents from "../Components/HotelsListComponents";
import SearchForm from "../Forms/HotelListForms";
import { connect } from "react-redux";
import {
  getHotelLists,
  getHotelListsByFilter,
  getHotelListsBySort
} from "../Actions/HotelsAction";

class HotelPage extends Component {
  onSort = name => {
    getHotelListsBySort(name);
  };

  searchHotelsByFacilities = values => {
    getHotelListsByFilter(values.name);
  };

  render() {
    return (
      <div className="hotels">
        <div className="container">
          <SearchForm onSubmit={this.searchHotelsByFacilities} />
          <HotelListComponents
            HotelList={this.props.HotelListState}
            onSort={this.onSort}
          />
        </div>
      </div>
    );
  }
}

export function mapStateToProps(state) {
  return {
    HotelListState: state.HotelListState
  };
}

export function mapDispatchToProps() {
  return {
    loadHotelListAction: getHotelLists()
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HotelPage);
