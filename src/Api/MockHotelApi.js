const delay = 500;
const mockData = [
  {
    name: "hotelone",
    starRating: 5,
    facilities: ["car park", "pool"]
  },
  {
    name: "hoteltwo",
    starRating: 3,
    facilities: ["car park", "gym"]
  },
  {
    name: "hotelthree",
    starRating: 3,
    facilities: []
  }
];

export let getHotels = () => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve(mockData);
    }, delay);
  });
};

export let getHotelsBySearch = Hotelsfacilities => {
  return new Promise((resolve, reject) => {
    if (!Hotelsfacilities) resolve(mockData);
    else {
      const searchResults = mockData.map(Hotels => {
        if (Hotels.facilities.find(hotel => hotel === Hotelsfacilities))
          return Hotels;
      });
      resolve(searchResults);
    }
  });
};



export let getHotelsBySort = sortName => {
    return new Promise((resolve, reject) => {
      if (!sortName) resolve(mockData);
      else {
        const searchResults = mockData.sort()
        resolve(searchResults);
      }
    });
  };