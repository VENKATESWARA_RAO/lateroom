// Enzyme.configure({ adapter: new Adapter() })
import { shallow, mount, configure } from "enzyme";
import * as React from "react";
import configureStore from "redux-mock-store";
import { Provider } from "react-redux";
import HotelListPage from "../../Pages/HotelListPage";
import { expect } from "chai";
const mockStore = configureStore();
import { MemoryRouter } from "react-router-dom";
import Adapter from "enzyme-adapter-react-16";

configure({ adapter: new Adapter() });

describe("HotelList Page", () => {
  let store;
  let wrapper;
  let mockHotels;

  beforeEach(() => {
    mockHotels = [
      {
        name: "hotelone",
        starRating: 5,
        facilities: ["car park", "pool"]
      }
    ];

    store = mockStore({
      HotelListState: mockHotels
    });
    store.dispatch = jest.fn();
    wrapper = mount(
      <Provider store={store}>
        <MemoryRouter>
          <HotelListPage />
        </MemoryRouter>
      </Provider>
    );
  });

  it("should contain div with class hotels HotelList", () => {
    expect(wrapper.find("div.hotels").length).to.be.equals(1);
  });

  it("should contain div with class container HotelList", () => {
    expect(wrapper.find("div.container").length).to.be.equals(2);
  });

  it('should contain HotelSearchForm Search', () => {
    // console.log(wrapper.debug());
    expect(wrapper.find('ReduxForm').length).to.be.equals(1);
});

it('should contain Hotels ', () => {
    expect(wrapper.find('HotelListComponents').length).to.be.equals(1);
});
});
