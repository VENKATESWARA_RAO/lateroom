import { expect } from "chai";
import * as React from "react";
import { shallow, mount, configure } from "enzyme";
import HotelsItemListComponents from "../../Components/HotelsItemListComponents";
import Adapter from "enzyme-adapter-react-16";

configure({ adapter: new Adapter() });

describe("AccountStatementList presentational component", () => {
  let properties;

  beforeEach(() => {
    properties = {
        HotelList: [
        {
          name: "hotelone",
          starRating: 5,
          facilities: ["car park", "pool"]
        }
      ]
    };
  });
  it('should have a `tr` element', () => {
    let wrapper = shallow(<HotelsItemListComponents {...properties} />);
    expect(wrapper.find('tr').length).to.be.equals(1);
});

it('should have a `td` element', () => {
    let wrapper = shallow(<HotelsItemListComponents {...properties} />);
    expect(wrapper.find('td').length).to.be.equals(3);
});
  
});
