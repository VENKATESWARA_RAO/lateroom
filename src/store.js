import { createStore } from "redux";
import { reducer } from "./reducers/RootReducer";

const store = createStore(reducer);

export default store;