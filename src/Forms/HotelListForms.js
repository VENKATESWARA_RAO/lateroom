import React from "react";
import { Field, reduxForm } from "redux-form";
import PropTypes from "prop-types";

const SearchForm = props => {
  const { handleSubmit } = props;
  return (
    <div className="container">
    
        <form className="form-inline" onSubmit={handleSubmit}>
        <div className="form-group">
            <Field
              name="name"
              component="input"
              type="text"
              placeholder="Search By Facilities"
              className="form-control"
            />
          </div>         

            <button type="submit" className="btn btn-primary">
              Search
            </button>
         
        </form>
      </div>
  
  );
};

export default reduxForm({
  form: "SearchForm" // a unique identifier for this form
})(SearchForm);
