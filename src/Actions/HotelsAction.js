import * as MockHotelApi from "../Api/MockHotelApi";
import store from "../store";

const hotelListSuccess = hotelListResponse => ({
  type: "LOAD_HOTELS_LISTS",
  HotelListState: hotelListResponse
});

export const getHotelLists = async () => {
  try {
    const hotelListResponse = await MockHotelApi.getHotels();
    store.dispatch(hotelListSuccess(hotelListResponse));
  } catch (error) {
    throw error;
  }
};

export const getHotelListsByFilter = async (SearchByFacility) => {    
      try {
          const hotelListResponse = await MockHotelApi.getHotelsBySearch(
            SearchByFacility);         
          store.dispatch(hotelListSuccess(hotelListResponse));
      } catch (error) {
          throw (error);
      }   
};


export const getHotelListsBySort= async (SortByColumn) => {    
  try {
      const hotelListResponse = await MockHotelApi.getHotelsBySort(
        SortByColumn);
     
      store.dispatch(hotelListSuccess(hotelListResponse));
  } catch (error) {
      throw (error);
  }   
};
