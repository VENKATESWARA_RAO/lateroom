import {  combineReducers } from 'redux';
import { reducer as reduxFormReducer } from 'redux-form';
import  HotelListState from './HotelsListReducer';

export const reducer = combineReducers({
    HotelListState,
    form: reduxFormReducer, 
  });